from django.http import HttpResponse, Http404
# from django.shortcuts import render
from django.views import generic

# Create your views here.
from django.views.decorators.csrf import csrf_exempt

from streams.models import Stream


class StreamView(generic.TemplateView):
    template_name = 'streams/stream.html'

    def get_context_data(self, **kwargs):
        context = super(StreamView, self).get_context_data()
        if 'stream' in kwargs:
            stream = Stream.objects.get(id=kwargs['stream'])
            if stream is None:
                return Http404('Oof')
            context['stream'] = stream
            return context
        else:
            return Http404('Oof')


class StreamListView(generic.ListView):
    model = Stream
    ordering = ['title']
    paginate_by = 10


@csrf_exempt
def on_publish(request):
    s = Stream.objects.first()
    s.live = True
    s.save()
    response = HttpResponse()
    response.status_code = 201
    return response


@csrf_exempt
def on_publish_done(request):
    s = Stream.objects.first()
    s.live = False
    s.save()
    response = HttpResponse()
    response.status_code = 201
    return response

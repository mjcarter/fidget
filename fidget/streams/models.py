import uuid

from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Stream(models.Model):
    url = models.CharField(max_length=255)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(null=True, to=User,
                                on_delete=models.CASCADE, blank=True)
    title = models.CharField(max_length=255, default='Default Stream Name')
    live = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.title} ({self.id})'
